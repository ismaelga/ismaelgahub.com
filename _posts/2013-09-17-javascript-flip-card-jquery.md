---
layout: post
title: "JavaScript flip card jQuery"
category: posts
---

These days I got to use this nice javascript library called [Flippant](http://labs.mintchaos.com/flippant.js/)
which lets you create a really nice functional flip card on an html
element. You might have seen this if on the OSX dashboard, when you
change a widget settings.

You can see the main website [http://labs.mintchaos.com/flippant.js/](http://labs.mintchaos.com/flippant.js/) or go ahead to the repository on github [https://github.com/mintchaos/flippant.js](https://github.com/mintchaos/flippant.js)


My only problem with it was using it on elements with relative position.
So you happen to have any issue like this here is the code I replaced to
make it work for me.

I basicaly changed the method "set_styles" that currently looks like
this

{% highlight js %}
function set_styles(back, front, position) {
  back.style.position = position
  back.style.top = front.offsetTop + "px"
  back.style.left = front.offsetLeft + "px"
  back.style['min-height'] = front.offsetHeight + "px"
  back.style.width = front.offsetWidth + "px"
  back.style["z-index"] = 9999
}
{% endhighlight %}


with the following.


{% highlight js %}
function set_styles(back, front, position) {
  back.style.position = position
  // back.style.top = front.offsetTop + "px"
  // back.style.left = front.offsetLeft + "px"
  back.style['min-height'] = front.offsetHeight + "px"
  back.style.width = front.offsetWidth + "px"
  back.style["z-index"] = 9999

  // HACK on library to get proper positioning
    var offsets = front.getBoundingClientRect();
    back.style.top = offsets.top + "px";
    back.style.left = offsets.left + "px";
  // \HACK
}
{% endhighlight %}





Basically it makes flippant use the real position of the elem calculated
by the browser.




Thanks for reading this :)
