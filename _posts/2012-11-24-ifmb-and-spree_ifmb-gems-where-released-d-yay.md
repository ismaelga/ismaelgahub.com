---
layout: post
title: "ifmb and spree_ifmb gems where released! :D yay"
category: posts
---


So over almost a week of work on them, they were released! But there still are improvements to be done on both.

But what matters is that if you rely on this service for payments you
can now use one of this gems to make your rails shop :) I say this
because we at [Sinemys](http://sinemys.com) had to implement some shops
in opencart because of the lack of any gem for this purpose. So we are
happy now that we can go all-in making e-commerce with
[Spree](http://spreecommerce.com) (the best e-commerce gem) :) 

You can check those gems in github: [ifmb](https://github.com/sinemys/ifmb) and
[spree_ifmb](https://github.com/sinemys/spree_ifmb)
