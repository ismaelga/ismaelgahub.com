---
layout: post
title: "Developing IFMB gem for spree"
category: posts
---

Background: At Sinemys we have some opencart php stores we want to move to Rails, because we prefer to maintain rails apps instead of php apps.

So the choice for a framework to create a store in Rails is simple:
Spreeee! Yep! But we have a problem, our client use a payment method
that’s not very common, a service provided by IfThen named [IfMb](https://www.ifthensoftware.com/ProdutoX.aspx?ProdID=5). So the issue of course is that there is no gem for such service and we are thinking on creating that gem. And this is the end of part 1 :) I will report any further work been done on this. But for now it seems a easy task since there are many implementations in PHP we can follow.
