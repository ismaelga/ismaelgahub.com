---
layout: page
title: "Projects from Ismael Abreu"
---
## Projects

##### [Descobre Localidade](http://descobrelocalidade.pt)


Small Sinatra app to help find the final word of a Portuguese kind of
Crosswords.



## Open-Source Contributions

#### [discourse/discourse](https://github.com/discourse/discourse/commits?author=ismaelga)
A platform for community discussion. Free, open, simple.

#### [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers/commits?author=ismaelga)
ActiveModel::Serializer implementation and Rails hooks

#### [peterc/multirb](https://github.com/peterc/multirb/commits?author=ismaelga)
Runs an IRB-esque prompt (but it's NOT really IRB!) over multiple Rubies using RVM or RBENV.

#### [strand/dude](https://github.com/strand/dude/commits?author=ismaelga)
Dude is a gem which improves errors by appending ' dude.' to exception messages.





